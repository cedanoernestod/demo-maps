/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Fragment, Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import env from './env.json';
import MapboxGL from '@react-native-mapbox-gl/maps';

class App extends Component {
  state = {
    markers: [
      {
        key: 'A',
        title: 'Englisch Garten',
        description: 'Familiar',
        latlng: [11.6033635, 48.1642323]
      },
      {
        key: 'B',
        title: 'Olimpya park',
        description: 'Familiar',
        latlng: [11.5496083, 48.1754646]
      },
      {
        key: 'C',
        title: 'Marienplatz',
        description: 'Familiar',
        latlng: [11.5732598, 48.1373932]
      },
      {
        key: 'D',
        title: 'Allianz Arena',
        description: 'Familiar',
        latlng: [11.6225185, 48.2187997]
      },
      {
        key: 'E',
        title: 'BMW Welt',
        description: 'Familiar',
        latlng: [11.5562963, 48.1771981]
      }
    ]
  }
  constructor(props) {
    super(props);
    MapboxGL.setAccessToken(env.accesstoken);
  }
  render() {
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <MapboxGL.MapView
          style={styles.map}
          >
          <MapboxGL.Camera
              zoomLevel={10}
              centerCoordinate={[11.576124, 48.137154]}
            />
            {this.state.markers.map(marker => (
              <MapboxGL.PointAnnotation
                id={marker.key}
                key={marker.key}
                coordinate={marker.latlng}
                title={marker.title}
                snippet={marker.description}
              />
            ))}
          </MapboxGL.MapView>
        </SafeAreaView>
      </Fragment>
    );
  }
};

const styles = StyleSheet.create({
  map: {
    width:'100%',
    height:800
  }
});

export default App;
