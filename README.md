# demo-maps

Demo maps for react native App

**Instructions**


**Building and installing dependencies**

First go to *maps* or *mapbox* directory then inside the folder run `cd ios && pod install` after that open the ios directory with Xcode and go to the tab "product" and select "build"

**Run app**
First install react-native `npm install -g react-native-cli`


Go to *maps* or *mapbox* directory and just run `react-native run-ios`
