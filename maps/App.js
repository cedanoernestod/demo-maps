/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, Component} from 'react';
import bmwLogo from './src/images/bmw.png'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import MapView, { Marker } from 'react-native-maps';
class App extends Component{
  state = {
    markers: [
      {
        key: 1,
        title: 'Englisch Garten',
        description: 'Familiar',
        latlng: {latitude:48.1642323, longitude: 11.6033635,}
      },
      {
        key: 2,
        title: 'Marienplatz',
        description: 'Familiar',
        latlng: {latitude:48.1373932, longitude: 11.5732598}
      },
      {
        key: 3,
        title: 'Olimpya park',
        description: 'Familiar',
        latlng: {latitude:48.1754646, longitude: 11.5496083}
      },
      {
        key: 4,
        title: 'Allianz Arena',
        description: 'Familiar',
        latlng: {latitude:48.2187997, longitude: 11.6225185}
      },
      {
        key: 5,
        title: 'BMW Welt',
        description: 'Familiar',
        latlng: {latitude:48.1771981, longitude: 11.5562963},
        image: bmwLogo
      },
    ]
  }
 render() {
  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
          <View style={styles.body}>
          <MapView
          initialRegion={{
            latitude: 48.137154,
            longitude:	11.576124,
            latitudeDelta: 0.1122,
            longitudeDelta: 0.1121,
          }}
          style={{width:'100%', height:780}}
          loadingEnabled={true}
          mapType="hybrid"
          showsUserLocation={true}
        >
         {this.state.markers.map(marker => (
          <Marker
            key={marker.key}
            coordinate={marker.latlng}
            title={marker.title}
            description={marker.description}
            image={marker.image}
          />
        ))}
        </MapView>
          </View>
      </SafeAreaView>
    </Fragment>
  );
 }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
